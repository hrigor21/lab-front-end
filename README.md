### Prerequisites

Prerequisites: Node.js (^10.12.0, or >=12.0.0) 

### Get started
```
  git clone https://gitlab.com/hrigor21/lab-front-end.git
  cd lab-front-end
  npm i
  npm start
```

### Put your files into /src folder

### For build

```
  npm run build
```

### For lint

```
  npm run lint      // lint js files
  npm run stylelint // lint css files
```
