const { randomUserMock, additionalUsers } = require("./mock_for_L3.js");

const lookup = require('country-code-lookup');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();



//Task1
function normalize(randomUserMock, additionalUsers) {
    let counter = 0;
    let normalized_array = randomUserMock.map(obj => {
        counter++;
        let newObj = new Object({
            gender: obj.gender,
            title: obj.name.title,
            full_name: obj.name.first + ' ' + obj.name.last,
            city: obj.location.city,
            state: obj.location.state,
            country: obj.location.country,
            postcode: obj.location.postcode,
            coordinates: obj.location.coordinates,
            timezone: obj.location.timezone,
            email: obj.email,
            b_date: obj.dob.date,
            age: obj.dob.age,
            phone: obj.phone,
            picture_large: obj.picture.large,
            picture_thumbnail: obj.picture.thumbnail,
            id: counter,
            favorite: false,
            course: '',
            bg_color: "#1f75cb",
        });
        return newObj;
    });

    let uniqueArr = normalized_array.filter(x => {
        if (undefined === additionalUsers.find(y => y.full_name === x.full_name))
            return x;
    });
    const result = uniqueArr.concat(additionalUsers);
    return result;

}

//Task2
function checkString(errorMessages, paramName, paramValue) {
    if (typeof (paramValue) !== 'string') {
        errorMessages.push(`Datatype of ${paramName} '${paramValue}' must be string.`);
        return false;
    } else if (paramValue.length === 0) {
        errorMessages.push(`${paramName} must be longer then 0.`);
        return false;
    } else if (paramValue[0].toUpperCase() !== paramValue[0]) {
        errorMessages.push(`First letter of ${paramName} '${paramValue}' must be upper case.`);
        return false;
    }
    return true;
}

function validate(user) {
    const errorMessages = [];
    checkString(errorMessages, 'full_name', user.full_name);
    checkString(errorMessages, 'gender', user.gender);

    checkString(errorMessages, 'note', user.note);
    checkString(errorMessages, 'state', user.state);
    checkString(errorMessages, 'city', user.city);

    if (typeof (user.age) !== 'number') {
        errorMessages.push(`Datatype of age '${user.age}' must be number.`);
    } else if (user.age < 19) {
        errorMessages.push(`Age '${user.age}' is incorrect, teacher must be older then 19 years old.`);
    }

    if (typeof (user.phone) === 'string' && checkString(errorMessages, 'country', user.country)) {
        const region = lookup.byCountry(user.country).iso2;
        const number = phoneUtil.parseAndKeepRawInput(user.phone, region);
        const validPhone = phoneUtil.isValidNumberForRegion(number, region);
        if (!validPhone) {
            errorMessages.push(`Phone '${user.phone}' is incorrect for ${user.country}.`);
        }
    }

    if (typeof (user.email) !== 'string') {
        errorMessages.push(`Datatype of email '${user.email}' must be string.`);
    } else if (!user.email.includes('@')) {
        errorMessages.push(`Email '${user.email}' must include symbol '@'.`);
    } else if (user.email.length < 5) {
        errorMessages.push(`Email '${user.email}' must contain at least 5 symbols.`);
    } else if (user.email[0] === '@') {
        errorMessages.push(`Email '${user.email}' can't start from symbol '@'.`);
    } else if (user.email[user.email.length - 1] === '@') {
        errorMessages.push(`Email '${user.email}' can't end on symbol '@'.`);
    }

    return errorMessages;
}

//Task3
function filter(arr, field, value) {
    return arr.filter(x => x[field] === value)
}

//Task4
function sort(arr, field, order) {
    let sortedArr = [];
    if (field === 'full_name' || field === 'country') {
        sortedArr = arr.sort((a, b) => a[field] > b[field] ? 1 : -1);
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    } else if (field === 'age') {
        sortedArr = arr.sort((a, b) => Number(a[field]) > Number(b[field]) ? 1 : -1);
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    } else if (field === 'b_day') {
        sortedArr = arr.sort((a, b) => new Date(a[field]) > new Date(b[field]) ? 1 : -1)
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    }
}

//Task5
function search(arr, field, value) {
    return arr.find(x => x[field] === value);
}

//Task6
function percent(usersArray, field, value) {
    const findedUsers = filter(usersArray, field, value);
    const percent = (findedUsers.length * 100) / usersArray.length;
    console.log(usersArray.length);
    return Math.round(percent);
}

console.log('----------Task1----------');
const arr = normalize(randomUserMock, additionalUsers);
console.log(arr);

console.log('----------Task2----------');
const user1 = {
    full_name: 'mr',
    gender: 'male',
    note: null,
    state: 'mecklenburg-Vorpommern',
    city: 'rhön-Grabfeld',
    country: 'germany',
    email: '@dsf',
    phone: "+49 6634712433"
};
const errorMessages1 = validate(user1);
console.log(errorMessages1);

console.log('----------Task3----------');
console.log(filter(arr, "gender", "male"));

console.log('----------Task4----------');
const arr3 = sort(arr, 'age', 'asc');
console.log(arr3);

console.log('----------Task5----------');
const obj = search(arr, 'age', 47);
console.log(obj);

console.log('----------Task6----------');
console.log(percent(arr, 'age', 47));